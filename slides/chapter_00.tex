\documentclass[size=screen,aspectratio=169]{beamer}
\usepackage{beamerthemeshadow,amsmath,multimedia,graphicx,multicol,tikz}
\begin{document}
\title{Chapter 00: Course Introduction}
\author{Instructor: John Labarga}
\date{January 17, 2023}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame} 

\section{Course Overview}
\begin{frame}
\frametitle{Goals of This Course}
\begin{itemize}
  \item Understand how to convert data into information, and present findings from analysis
  \item Develop a critical and ``data-driven'' mindset
  \item Gain a fundamental understanding of probability 
  \item Learn to use statistical tools for decision-making
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{General Schedule}
\begin{itemize}
\item The course is broken down into three units:
  \begin{itemize}
  \item Descriptive statistics and regression: January - mid-February
    (Chapters 1-3)
  \item Probability: mid-February - March (Chapters 5-8)
  \item Inference: April (Chapters 9-13)
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Chapter Dependencies}

  \begin{tikzpicture}[main/.style = {draw, circle}]
  \node[main] at (-1.2, 1.6) (1) {X};
  \node[main] (2) [right of=1] {Y};
  \node[draw,text width=4.5cm] at (-2, 2) {\underline{Legend}
    \vspace{3mm} \\Chapter Y uses\\
    material from \\chapter X};
  % edges
  \draw[->] (1) -- (2);
\end{tikzpicture}
  
  \begin{center}
    \begin{tikzpicture}[node distance={13mm}, main/.style = {draw, circle}] 
  \node[main] (1) {1};
  \node[main] (2) [right of=1] {2};
  \node[main] (3) [below right of=2] {3}; 
  \node[main] (5) [above right of=2] {5}; 
  \node[main] (6) [right of=2] {6}; 
  \node[main] (7) [right of=6] {7}; 
  \node[main] (8) [right of=7] {8}; 
  \node[main] (9) [right of=8] {9}; 
  \node[main] (10) [above right of=9] {10}; 
  \node[main] (11) [below right of=9] {11}; 
  \node[main] (12) [right of=10] {12}; 
  \node[main] (13) [right of=11] {13}; 

  % edges
  \draw[->] (1) -- (2);
  \draw[->] (2) -- (3);
  \draw[->] (2) -- (5);
  \draw[->] (5) -- (6);
  \draw[->] (6) -- (7);
  \draw[->] (7) -- (8);
  \draw[->] (8) -- (9);
  \draw[->] (9) -- (10);
  \draw[->] (10) -- (11);
  \draw[->] (9) to [out=90, looseness=1.5] (12);
  \draw[->] (12) -- (13);
\end{tikzpicture}
\end{center}
\end{frame}

\section{Syllabus Highlights}
\begin{frame}
\frametitle{Grading}
\begin{itemize}
  \item WebAssign homework assignments: 20\%
  \item Minitab lab assignments: 10\%
  \item Exams 
    \begin{itemize}
      \item Exam 1: 20\%
      \item Exam 2: 20\%
      \item Final Exam: 30\%
    \end{itemize}
    
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Exams}
\begin{itemize}
  \item The first two exams focus only on material from their respective units.
  \item The final exam is cumulative, but with a focus on material since exam 2.
  \item Exams are multiple choice.
  \item Dates:
    \begin{itemize}
      \item Exam 1: February 21
      \item Exam 2: March 28
      \item Final Exam: TBD
    \end{itemize}

  \item Let me know ASAP of any midterm conflicts. Midterms not taken,
    without prior agreement of rescheduling, will receive a score of
    0.
  \item Conflicts with the final exam must be resolved with OSS.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Late Work}
\begin{itemize}
  \item Minitabs: 10\% penalty per week late
  \item WebAssigns: 50\% penalty per unit late.
  \item Both close automatically, so you must let me know if you
    want to turn in late work.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Required Materials}
\begin{itemize}
  \item Textbook: \emph{Mind on Statistics} by Utts
  \item WebAssign key
  \item A graphing calculator (TI-83/84)
\end{itemize}
\end{frame}

%\begin{frame}
%\frametitle{Ways To Satisfy Required Materials}
%\begin{itemize}
%  \item Physical textbook available at the bookstore.
%  \item The WebAssign key is available from Cengage through the course site.
%  \item Cengage also offers an e-book with their WebAssign key (note:
%    only available until the end of the course)
%\end{itemize}
%\end{frame}

\section{What is statistics?}
\begin{frame}
\begin{itemize}
\item In common parlance, a ``statistic'' is a number that summarizes some process, entity, etc.
  \begin{itemize}
  \item Country X has a population of Y people
  \item X\% of shoppers at a store buy product Y
    \item The mean August temperature in city X is Y degrees
    \end{itemize}
\item This can lead to the misconception that the field of statistics
  is about computing these numbers
\item Statistics is actually about working with and drawing
  conclusions from data, especially generalizing from small samples to
  large populations
\item To do this, statistics combines data analysis, probability
  theory, and computing
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Historical Background}
\begin{itemize}
\item The term ``statistics'' has its roots in the use of data to
  quantify properties of states (i.e. countries)

  $$\mbox{\emph{statisticum collegium}} \rightarrow \mbox{\emph{statistik}} \rightarrow \mbox{statistics}$$

\item Statistics has its roots in three main disciplines:
  \begin{itemize}
  \item Data analysis (collection and interpretation)
  \item Probability theory
  \item Computational data processing
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Origins of Data Collection and Interpretation}
\begin{itemize}

  \item States (i.e. countries) have collected data about their
    populations and territories for thousands of years.

  \item There is data on land use in Uruk in \emph{The Epic of
    Gilgamesh}, which dates to ca. 2100 BCE.

  \item After the Norman invasion, William the Conqueror ordered
    assessors to compile \emph{The Domesday Book}, which assessed the
    value of all land in England.

  \item By the 18th century, countries were conducting censuses on
    their populations, and measuring installed manufacturing
    capacity. By the early 20th century, they were measuring their
    economies with metrics like GDP.

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Origins of Probability Theory}
\begin{itemize}

  \item Mathematicians began formally studying probability in the 17th
    century to understand games of chance and odds during gambling.

  \item During the 18th and 19th centuries this developed into a
    framework for working with random objects in general, which found
    uses in mathematics and the sciences.

  \item Probabilistic modeling and prediction developed during the
    20th century to forecast uncertain future events.

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Origins of Computational Data Processing}
\begin{itemize}

  \item Early mechanical computers were built in the 19th century to
    tabulate large scale surveys (e.g. the US Census), and to solve
    algebraic problems (e.g. the difference engine).

  \item Programmable electronic computers were developed in the early
    20th century, mainly as code-breaking and industrial planning
    tools.

  \item By the late 20th century these computers were cheap and could
    perform statistical calculations, run business software, etc.

  \item 21st century computers became fast enough to build complex
    statistical models, e.g. image recognition systems, chatbots, etc.

\end{itemize}
\end{frame}


\end{document}

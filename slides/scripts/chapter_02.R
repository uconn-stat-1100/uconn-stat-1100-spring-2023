# first lady histogram example

first_lady_lifetimes <- c(71, 74, 34, 81, 62, 77, 61, 36, 89, 52, 69, 88, 64, 
                          55, 57, 73, 64, 66, 76, 58, 86, 43, 83, 60, 60, 87, 82, 54, 89, 64, 78, 70, 78, 97, 83, 65, 81)

png("../figures/ch02/07_histogram.png", width = 866, height = 565)

hist(first_lady_lifetimes, breaks = c(30,39,49,59,69,79,89,100), freq=TRUE)

dev.off()

\documentclass[size=screen,aspectratio=169]{beamer}
\usepackage{beamerthemeshadow,amsmath,multimedia,graphicx,multicol}
\begin{document}
\title{Chapter 02: Descriptive Statistics}
\author{Instructor: John Labarga}
\date{January 17, 2023}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame} 

\section{Populations vs. samples}
\begin{frame}
\frametitle{Sampling and Inference}
\begin{itemize}
\item The central activity of statistics is trying to make a statement
  about a population (a large group of objects or situations) from a
  (relatively) small sample
\item Our research questions can usually be phrased in terms of a
  statement about a population characteristic
\item Example: developing a new drug to treat illness X
  \begin{itemize}
  \item Population: all people with illness X
  \item Research question: what proportion of people with this illness
    are cured by the new drug?
  \end{itemize}
  \item Taking a measurement on every object in a population is called
    a \emph{census}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Why not always take a census?}
\begin{itemize}
\item Most interesting populations are large, making a census
  expensive and impractical
\item A census is sometimes impossible for ethical reasons
  \begin{itemize}
  \item E.g. drug development, a ``census'' would mean forcing everyone
    to take the new drug, even if you're not sure that it works
  \end{itemize}
\item In some contexts a census is not even possible, because your
  population includes objects that don't exist
  \begin{itemize}
  \item E.g. trying to predict future sales in a store, or determine
    the best action at a given step in a poker game
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The Sampling-Inference Cycle}
\begin{itemize}
\item Since a census is usually impossible, we want to learn about the
  population from a small subset thereof, called a sample
\item A sample is a set of randomly-selected objects from the
  population
\item If the sample is randomly-selected, and not too small, then
  characteristics measured on the sample should approximate the
  corresponding characteristics in the population
\item The process of randomly choosing a sample from the population is
  called sampling
\item The process of generalizing back to the population from a sample
  is called inference
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The Sampling-Inference Cycle}
\begin{itemize}
\item A characteristic measured on a sample is called a \textbf{statistic}
\item The corresponding characteristic measured on the population is
  called a \textbf{parameter}
\item If the sample is drawn from the population correctly, then the
  statistic's value should approximate the parameter's value
\item Statistics are usually denoted with Latin letters, parameters
  are usually denoted with Greek letters (e.g. $\theta$, $\phi$)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The Sampling-Inference Cycle}
\begin{itemize}
\item Example: trying to figure out the average height of all the trees
  in a forest
  \begin{itemize}
  \item A census is probably impossible, you'd have to measure the
    height of every tree in the forest then average the results
  \item Instead take a random selection of trees, measure their
    heights, and find the average
  \item If the trees were randomly selected, the average height of
    trees in your sample should be about the same as the average
    height of the trees in the forest
  \item This is a statistic (average height of trees in your sample)
    approximating a parameter (average height of trees in the forest)
  \end{itemize}
\end{itemize}
\end{frame}


\section{Components of a dataset}
\subsection{Variables and observations}
\begin{frame}
\frametitle{Dataframes}
\begin{itemize}
\item The most basic type of dataset is \emph{tabular} data, where data is presented in a dataframe
\item An observation is a entity on which you measured several characteristics
\item A variable is a measurement of a characteristic across several observations
\item Several variable across several observations constitutes a dataset
\end{itemize}

\vspace{4mm}

\begin{tabular}{l | c | c | c | c }
Observation Name & Variable1 & Variable2 & Variable3 & Variable4 \\
\hline \hline
Observation 1 & X1 & X2 & X3 & X4 \\ 
Observation 2 & Y1 & Y2 & Y3 & Y4\\
Observation 3 & Z1 & Z2 & Z3 & Z4 
\end{tabular}
\end{frame}

\begin{frame}
\frametitle{Other Dataset Types}
\begin{itemize}
\item Most data is less structured than dataframes, which are by
  nature highly structured
\item Examples of ``unstructured'' data:
  \begin{itemize}
  \item Images
  \item Video
  \item Text
  \item Audio
    \end{itemize}
\end{itemize}
\end{frame}

\subsection{Types of variables}
\begin{frame}
\frametitle{Qualitative (Categorical) Variables}
\begin{itemize}
\item Some attributes of an object or observation naturally fall into
  one of several defined categories
  \item Examples of categorical variables:
  \begin{itemize}
  \item A person's town of residence
  \item Color of a car
  \item Whether or not someone smokes
  \end{itemize}
  \item The possible categories should be listable
  \item Generally a categorical variable can only take on one category
    at a time
      \begin{itemize}
  \item If this is not the case, the variable is called
    ``multi-label''
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Ordinal Categorical Variables}
\begin{itemize}
\item Ordinal variables are a subtype of categorical variables: all
  ordinal variables are categorical, but not all categorical variables
  are ordinal
\item Ordinal labels are used to encode a \emph{scale} that sorts
  values in some way (e.g. from best to worst)
\item Examples:
  \begin{itemize}
  \item Letter grades in a course (A to F, sorted best to worst)
  \item Education level (Elementary school diploma to PhD, sorted
    least to most)
  \end{itemize}
\item Note that ordinal variables are sometimes written as numbers,
  but this does not make them numerical (e.g. a restaurant being rates
  from 1 to 5 stars)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Quantitative (Numerical) Variables}
\begin{itemize}
\item Any property of an observation that is numerically measurable is
  a quantitative variable.
  \item Examples of quantitative variables:
  \begin{itemize}
  \item A person's height/weight
  \item How many students attend UConn
  \item The GDP of a country in the local currency
  \end{itemize}
  \item The boundaries of possible values are often defined
    (e.g. height and weight must be positive numbers, and are
    theoretically limited on the upper end), but this is not a
    requirement
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Discrete Quantitative Variables}
\begin{itemize}
\item If a quantitative variable is inherently bound to some set, it is discrete
\item Example: the number of siblings someone has must be an integer,
  they cannot have 3.14 siblings
\item Common binding sets are the integers or some scaled version
  thereof (e.g. money in \emph{cents} is bound on the integers, so
  money in \emph{dollars} is bound on $ \$\dots XX.XX $)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Continuous Quantitative Variables}
\begin{itemize}
\item If a quantitative variable is not discrete, it's continuous
  \item Examples:
  \begin{itemize}
  \item Your true weight ($145.88975897894\dots$ pounds)
  \item True time (some process took $17.786943\dots$ seconds)
  \item Most measurable natural quantities
  \end{itemize}
\item We often \emph{discretize} continuous variables by reducing their precision
\item E.g. a doctor just records your height as $XX.X$ inches, they
  don't measure it to a very high level of precision
\item These quantities are still truly continuous, but we've made them
  discrete by deciding how many significant digits we'll measure and
  store them to
\end{itemize}
\end{frame}

\subsection{Distributions}
\begin{frame}
\frametitle{Basic properties of quantitative variables}
\begin{itemize}
\item \emph{Location}: the range where the data tends to be,
  e.g. adult heights are positive numbers between 40 and 100 inches
\item \emph{Center}: where is the ``middle'' value of the data,
  e.g. what's the average adult height?
\item \emph{Shape}: what does a plot of the data look like? Is it
  flat, bell-shaped, symmetric, etc.?
\item These are all properties of the \emph{distribution} of the
  variable
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{What is a distribution?}
\begin{itemize}
\item The \emph{distribution} of a variable measures how many members
  of a population or dataset fall into various ranges along the
  location of the data
\item We'll examine distributions in more depth in the probability
  unit; for now you can think of it as a plot of the data that shows
  its shape
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Features of Distributions}
\begin{itemize}
\item \emph{Skewness}: is the distribution symmetric, or skewed to one
  side?
\item \emph{Modality}: is the distribution flat? Does it have a single
  peak or several?
\end{itemize}

\begin{center}
  \includegraphics[width=3in]{./figures/ch02/01_skewness.png}
  \end{center}
\end{frame}

\section{Summarizing Categorical Data}
\subsection{Numerical Summaries}
\begin{frame}
\frametitle{Frequency Tables}
\begin{itemize}
\item Since categorical data consists of a finite (and often small)
  number of labels, you can just tally them to produce a
  \emph{frequency table}.
\item You can then plot this frequency table as a bar chart to give a
  visual sense of the frequencies of different categories.
\end{itemize}

\begin{center}
\begin{tabular}{l | c | }
Label Name & Tally  \\
\hline \hline
Label 1 & Tally of Label 1 \\ 
Label 2 & Tally of Label 2\\
Label 3 & Tally of Label 3 
\end{tabular}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Frequency Table Example}

Suppose students' grades at the end of a course are as follows:\\

\begin{center}
  A, A, B, A, C, B, A, D, A, B, B, C, A, D, C, A, C, C, C, A
\end{center}

The frequency table for these grades is:

\begin{center}
\begin{tabular}{l | c | }
Grade & Frequency  \\
\hline \hline
A & 8 \\ 
B & 4 \\
C & 6 \\
D & 2 \\
\hline
Total & 20
\end{tabular}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Relative Frequency Tables}

In some contexts, the raw count is less useful than the
\emph{percentage} of each category. In that case, each frequency is
divided by the total to produce a relative frequency.

\vspace{2mm}

The frequency table of the data on the previous slide is:

\begin{center}
\begin{tabular}{l | c | }
Grade & Relative Frequency  \\
\hline \hline
A & 0.4 \\ 
B & 0.2 \\
C & 0.3 \\
D & 0.1 \\
\end{tabular}
\end{center}

\end{frame}

\subsection{Visual Summaries}
\begin{frame}
\frametitle{Pie Charts}
\begin{itemize}
\item The relative frequency of each category is displayed as the area of a slice of a circle.
\end{itemize}

\begin{center}
  \includegraphics[width=3in]{./figures/ch02/02_pie_chart.png}
\end{center}

\end{frame}


\begin{frame}
\frametitle{Pie Charts}
\begin{itemize}
\item You can compare two populations with side-by-side pie charts.
\end{itemize}

\begin{center}
  \includegraphics[width=3in]{./figures/ch02/03_multi_pie_chart.png}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Bar Charts}
\begin{itemize}
\item List the categories on the horizontal axis.
  \begin{itemize}
  \item Ordinal variables should be sorted according to their scale.
  \end{itemize}
\item Display frequency (or relative frequency) as the height of
  side-by-side bars instead.
\item Visually easier to compare heights of bars than areas of circles.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example: UConn Ranking}

  1000 students were asked: ``how does UConn rank among the colleges to which you applied?''
  
\begin{center}
  \includegraphics[width=3in]{./figures/ch02/04_bar_chart.png}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Clustered Bar Charts}

\begin{itemize}
\item Many datasets contain multiple categorical variables.
\item If you suspect that there may be a relationship between these
  categorical variables, you should first investigate this visually.
\item Making a clustered bar chart:

  \begin{itemize}
  \item Choose one variable to be the ``inner'' variable, and the
    other to be the ``outer'' variable.
  \item For each level of the outer variable, make a mini-bar chart of
    the inner variable.
  \item Label the horizontal axis with the levels of the outer variable.
  \\item Place the mini-bar charts side by side.
  \end{itemize}

\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Example: Nighttime Lighting Conditions and Myopia}
\begin{itemize}
\item After years of experience, a pediatrician may suspect that
  children who sleep in lit rooms are more likely to develop
  nearsightedness (myopia).
\item The pediatrician collects data in this format:
\end{itemize}

\begin{center}
\begin{tabular}{l | c | c | }
Child ID & Infant Lighting Conditions & Myopia Level \\
\hline \hline
1 & Nightlight & High \\ 
2 & Dark & None \\
3 & Full Light & Some \\
4 & Full Light & None \\
\end{tabular}

\dots

\end{center}
\end{frame}

\begin{frame}
\frametitle{Example: Nighttime Lighting Conditions and Myopia}

\begin{center}
  \includegraphics[angle=90,width=3.5in]{./figures/ch02/05_clustered_bar_chart.png}
\end{center}
\end{frame}

\section{Summarizing Quantitative Data}
\subsection{Visual Summaries}
\begin{frame}
  \frametitle{Visual Properties of Quantitative Data}
\begin{itemize}
\item We hope to learn several things about the distribution of
  quantitative data from a plot:

  \begin{itemize}
  \item Spread: is the data tightly clustered around one or more
    values, or evenly spread along its range?
  \item Shape: does the distribution look symmetric or skewed? If
    skewed, in which direction?
  \item Modality: does the data look unimodal, bimodal, etc.?
  \item Outliers: do any point stick out as inconsistent with the rest
    of the data?
  \end{itemize}

  \item A good visual summary of quantitative data should communicate
    all of this information.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Dotplots}
\begin{itemize}
\item Good for summarizing small- to medium-sized samples of
  \underline{discrete quantitative} data.
\item Making a dotplot:
  \begin{itemize}
  \item Draw a number line that runs the range of your dataset.
  \item Go through the dataset, drawing a dot above the number line at
    each value seen.
  \item When there are duplicates of a value, vertically stack the
    dots for that value.
  \end{itemize}

\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Example: Dotplot of NFL QB Ages}

Consider this sample of nineteen NFL quarterbacks' ages:

34, 31, 31, 28, 40, 26, 28, 30, 33, 30, 29, 25, 29, 27, 27, 34, 27, 28, 32

\vspace{3mm}

Dotplot of this data:

\begin{center}
  \includegraphics[width=4in]{./figures/ch02/06_dotplot.png}
\end{center}    
\end{frame}

\begin{frame}
\frametitle{Histograms}

\begin{itemize}

\item Histograms give a sense of the distribution's overall shape and spread.
\item Histograms work best with medium to large sets of continuous
  data, but can also be used with discrete data.
\item Making a histogram:

  \begin{itemize}
  \item Segment the range of the data into $n$ equal-sized intervals
  \item Tally the number of observations that fell into each interval
  \item Make a bar chart, using the intervals as your ``categories''
  \end{itemize}

\item Choosing the number of intervals correctly is important: having
  either too few or too many reduces the amount of detail in the plot.

\item Modern statistical software packages use heuristics to
  automatically choose a good number of intervals.

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example: Histogram of First Lady Lifespans}

Consider this sample of lifespans of first ladies of the US:

71, 74, 34, 81, 62, 77, 61, 36, 89, 52, 69, 88, 64, 55, 57, 73, 64, 66, 76, 58, 86, 43, 83, 60, 60, 87, 82, 54, 89, 64, 78, 70, 78, 97, 83, 65, 81

\vspace{2mm}

Our range is 34-97; to make things easier, let's round the range to 30-100.

We'll use 7 intervals, so that each interval corresponds to a decade.

\end{frame}

\begin{frame}
\frametitle{Example: Histogram of First Lady Lifespans}

Tallying the data into intervals:

\begin{center}
\begin{tabular}{l | c |}
Interval & Tally \\
\hline \hline
30-39 & 2 \\ 
40-49 & 1 \\
50-59 & 5 \\
60-69 & 10 \\
70-79 & 8 \\
80-89 & 10 \\
90-99 & 1 \\
\end{tabular}
\end{center}

\end{frame}

\begin{frame}
  \frametitle{Example: Histogram of First Lady Lifespans}

\begin{center}
  \includegraphics[width=4in]{./figures/ch02/07_histogram.png}
\end{center}    
  
\end{frame}


\begin{frame}
  \frametitle{Boxplots}
\begin{itemize}
\item Appropriate for either discrete or continuous data.
\item Visually displays the major statistics of the sample, and calls
  attention to outliers.
\item Communicates skewness well; lack of symmetry is often more
  obvious on a boxplot than a histogram.
\item Histograms and boxplots are often show together on the same
  sample, to get a full picture of the distribution.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Digression: Percentiles}
\begin{itemize}
\item Several of the statistics plotted by the boxplot are
  percentiles.
\item The $k^{\mbox{th}}$ percentile of a sample/population is a value
  such that $k$\% of the sample/population fall below the
  $k^{\mbox{th}}$ percentile, and $(100-k)$\% of the sample/population
  values fall above the $k^{\mbox{th}}$ percentile.
\item E.g. if your score on an exam s at the 90th percentile, then
  90\% of people did worse than you, and 10\% did better.
\item Percentiles provide a measurement of the distribution shape by
  indicating how much o the sample/population is to the left and
  right of a given value.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The Median: a Special Percentile}
\begin{itemize}
\item The median of a dataset is a point such that 50\% of the dataset
  is less than that point, and 50\% of the dataset is greater than
  that point.
\item Computing the median is straightforward, and depends on whether
  the sample size $n$ is even or odd. First, sort the data from
  smallest to largest. Then:
  \begin{itemize}
  \item If $n$ is odd, the number in the middle is the median.
    \item If $n$ is even, the means of the two numbers in the middle
      is the median
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example: Computing the Median}

  Consider the dataset:

  91, 79, 60, 94, 89, 93, 86

  \vspace{4mm}

  Sorted, this dataset is:

  60, 79, 86, 89, 91, 93, 94

  \vspace{4mm}

  The middle entry is 89, so 89 is the median of this dataset.
  
\end{frame}

\begin{frame}
  \frametitle{Example: Computing the Median}

  Suppose we add 75, so that out new dataset is:

  91, 79, 60, 94, 89, 93, 86, 75

  \vspace{4mm}

  Sorted, this dataset is:

  60, 75, 79, 86, 89, 91, 93, 94

  \vspace{4mm}

  The two middle values are 86 and 89. The average of 86 and 89 is
  87.5, so 87.5 is the median of this dataset.
  
\end{frame}

\begin{frame}
  \frametitle{Computing Percentiles: a Manual Approximation}

  All statistical software and most graphing calculators can compute percentiles.

  However, they are difficult to precisely compute manually, so we have
  a manual approximation:

  \begin{enumerate}
  \item If you are looking for the kth percentile, compute $p = \frac{k}{100}$
  \item Compute $n \cdot p$, where $n$ is the sample size
  \item Round $n \cdot p$ up to get $m$
  \item Sort the data
  \item The mth entry in the sorted dataset is your percentile
  \end{enumerate}
  
  
\end{frame}


\begin{frame}
  \frametitle{Quartiles: More Special Percentiles}

  The 25th and 75th percentiles are called the first and third
  quartiles, and denoted $Q_1$ and $Q_3$.

  \vspace{4mm}

  Let's compute $Q_1$ and $Q_3$ for this dataset:

  91, 79, 60, 94, 89, 93, 86

  \vspace{4mm}

  First, sort it:

  60, 79, 86, 89, 91, 93, 94

  \vspace{4mm}

  $Q_1$: $n \cdot p = 7 \cdot 0.25 = 1.75$, which rounds up to 2. So $Q_1 = 79$.

  $Q_3$: $n \cdot p = 7 \cdot 0.75 = 5.25$, which rounds up to 6. So $Q_3 = 93$.
    
\end{frame}


\begin{frame}
  \frametitle{Boxplots}

  The boxplot visually displays the five number summary: the median,
  $Q_1$, $Q_3$, the lower adjacent value, and the upper adjacent
  value. It also plots outliers.

  \vspace{4mm}

  \emph{Note}: in some online resources, you will see the five number
  summary given as the sample minimum, sample maximum, median, $Q_1$,
  and $Q_3$. This is not the definition we will use in this course.
  
\end{frame}

\begin{frame}
  \frametitle{Boxplots}

  Constructing a boxplot:

  \begin{enumerate}
  \item Draw a box whose lower end is at $Q_1$, and whose upper end is at $Q_3$.
  \item Draw a vertical line at the median.
  \item Calculate IQR $= Q_3 - Q_1$
  \item Lower fence: $Q_1 - 1.5\mbox{IQR}$
  \item Upper fence: $Q_3 + 1.5\mbox{IQR}$
  \item Lower adjacent value: the smallest point in the dataset that's
    still larger than the lower fence. Draw a line from $Q_1$ to this
    value.
  \item Upper adjacent value: the largest point in the dataset that's
    still smaller than the upper fence. Draw a line from $Q_3$ to this
    value.

  \item Points outside the fences are outliers.

  \end{enumerate}
    
\end{frame}

\begin{frame}
  \frametitle{Example: Boxplots}

  Consider this dataset, which contains the weights of members of the
  Cambridge crew team:

  188.5, 183, 194.5, 185, 214, 203.5, 186, 178.5, 109

  The five number summary for this dataset is:

  \begin{itemize}
  \item Median: 186
  \item $Q_1$: 180.75
  \item $Q_3$: 199
  \item LAV: 178.5
  \item UAV: 214
  \end{itemize}

  Note that the IQR is $199-180.75 = 18.25$, so the lower fence is
  $180.75 - 1.5 \cdot 18.25 = 153.375$ and the upper fence is $199 +
  1.5 \cdot 18.25 = 226.375$.
    
\end{frame}

\begin{frame}
  \frametitle{Example: Boxplots}

  The boxplot for the Cambridge crew team looks like this:

  \begin{center}
  \includegraphics[width=4in]{./figures/ch02/08_boxplot.png}
  \end{center}

  The point 109 is an outlier.
      
\end{frame}


\begin{frame}
  \frametitle{Addressing Outliers}

  Generally, outliers have three possible origins:

  \begin{itemize}
  \item The outlier is a legitimate, extreme data value
  \item A mistake was made taking a measurement or entering it into
    storage
  \item The observation in question belongs to a different group than
    the bulk of data observed
  \end{itemize}
      
\end{frame}

\begin{frame}
  \frametitle{Distribution Shape}

  A symmetric distribution looks about the same on either side of the
  center:

  \begin{center}
  \includegraphics[width=3in]{./figures/ch02/09_symmetric.png}
  \end{center}
      
\end{frame}

\begin{frame}
  \frametitle{Distribution Shape}

  A left (negatively) skewed distribution has a tail on its left (negative) side:

  \begin{center}
  \includegraphics[width=3in]{./figures/ch02/10_left_skew.png}
  \end{center}
      
\end{frame}

\begin{frame}
  \frametitle{Distribution Shape}

  A right (positively) skewed distribution has a tail on its right (positive) side:

  \begin{center}
  \includegraphics[width=3in]{./figures/ch02/11_right_skew.png}
  \end{center}
      
\end{frame}

\begin{frame}
  \frametitle{Shape's Influence on the Mean and Median}

  Notice that while the mean's calculation involves every point in the
  dataset, the median's calculation only involves a few points near
  the center of the \emph{sorted} dataset.

  \vspace{2mm}

  The mean will generally be more affected by outliers than the
  median, and will tend to be drawn away from the median towards the
  outliers.

  \vspace{2mm}

  Thus, broadly:

  \begin{itemize}
  \item Median $\approx$ Mean: approximately symmetric
  \item Mean $>$ Median: right skewed
  \item Mean $<$ Median: left skewed
  \end{itemize}

  
\end{frame}

\subsection{Numerical Summaries}
\begin{frame}
  \frametitle{Center}
\begin{itemize}
\item As previously discussed, the mean and median are the primary
  measures of the center of a distribution.
\item Which you use to measure a distribution's center depends on how
  much you want your measure to include outliers and extreme values.
  \item E.g. personal income is heavily right skewed, so the median
    gives a better sense of the income of a ``typical'' person than
    the mean.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Variation and Dispersion}
\begin{itemize}
\item Range: $\mbox{max} - \mbox{min}$
\item IQR
\item Variance: a form of average distance from points to the mean,
  more spread out distributions will have a higher variance
\item Standard deviation: square root of the variance 
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Formulas for the Sample Variance and Standard Deviation}

  Variance:

  $$s^2 = \sum_{i=1}^n \frac{(x_i - \bar{x})^2}{n-1}$$

  Standard Deviation:

  $$s = \sqrt{\sum_{i=1}^n \frac{(x_i - \bar{x})^2}{n-1}}$$
  
\end{frame}

\begin{frame}
  \frametitle{Example: Computing the Variance and Standard Deviation}

  Consider this dataset:

  20, 30, 40, 50, 60, 70

  Note that $\bar{X} = 45$

  \begin{center}
  \begin{tabular}{| c | c | c |}
  $X_i$ & $(X_i - \bar{X})$ & $(X_i - \bar{X})^2$ \\
  \hline
  20 & -25 & 625\\ 
  30 & -15 & 225\\
  40 & -5 & 25\\
  50 & 5 & 25\\
  60 & 15 & 225\\
  70 & 25 & 625\\
  \hline
   & & 1750
  \end{tabular}
  \end{center}

  So we have $\sum_{i=1}^n (X_i - \bar{X})^2 = 1750$.
  Then $s^2 = \frac{1750}{6-1} = 350$, and $s = \sqrt{350} \approx 18.71$.
  
\end{frame}

\begin{frame}
  \frametitle{Population Variance and Standard Deviation}

  The population variance is denoted $\sigma^2$, and is:

  $$\sigma^2 = \sum_{i=1}^N \frac{(X_i - \mu)^2}{N}$$

  The population standard deviation is:

  $$\sigma = \sqrt{\sum_{i=1}^N \frac{(X_i - \mu)^2}{N}}$$
  
\end{frame}

\begin{frame}
  \frametitle{Why $n-1$?}

  Notice that the main difference between the population and sample
  variances is that the sample variance has $n-1$ in the
  denominator. The population variance has $N$.

  \vspace{2mm}

  A sample will generally have less variation than the population,
  being a small subset of the population. Therefore we need to boost
  its variance a bit so that it approximates the population variance.

  \vspace{2mm}

  It turns out that the ideal way to do this is to reduce the
  denominator by 1.
  
\end{frame}

\begin{frame}
  \frametitle{Bell-shaped Distributions}

  Many quantities in nature have bell-shaped distributions, which look
  like this:

  \begin{center}
  \includegraphics[width=3in]{./figures/ch02/12_bell_shaped.png}
  \end{center}
  
\end{frame}


\begin{frame}
  \frametitle{Bell-shaped Distributions}

  \begin{itemize}
  \item The defining property of these distributions is their
    symmetry, i.e. it's equally probable that a sample from this
    distribution will be to the left of the mean as to the right.
  \item The normal distribution is the most widely-used bell-shaped
    distribution.
  \item The normal distribution has a variety of convenient properties
    that we will make heavy use of.
    \item The normal distribution has two parameters: $\mu$ (mean) and
      $\sigma$ (standard deviation).
  \end{itemize}
  
\end{frame}


\begin{frame}
  \frametitle{The Empirical Rule}

  \begin{itemize}
  \item The empirical rule is a useful property of the normal
    distribution.
  \item It consists of three statements that relate intervals around
    the mean, in increments of the standard deviation, to the
    proportion of the population in those intervals:

    \begin{itemize}
      \item 68\% of the values fall within one standard deviation of
        the mean ($[\mu - \sigma, \mu + \sigma]$)
      \item 95\% of the values fall within two standard deviations of
        the mean ($[\mu - 2\sigma, \mu + 2\sigma]$)
      \item 99.7\% of the values fall within three standard deviations
        of the mean ($[\mu - 3\sigma, \mu + 3\sigma]$)
    \end{itemize}
    
  \end{itemize}
  
\end{frame}


\begin{frame}
  \frametitle{Example: The Empirical Rule}

  Suppose adult female waist circumference is normally distributed,
  with mean 36 inches and standard deviation 3.5 inches.

  \begin{center}
  \includegraphics[width=3.5in]{./figures/ch02/13_normal.png}
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{Example: The Empirical Rule}

  What percentage of adult females have waist circumferences between
  32.5 inches and 39.5 inches?

  \begin{center}
  \includegraphics[width=3.5in]{./figures/ch02/14_1sigma.png}
  \end{center}
  
\end{frame}


\begin{frame}
  \frametitle{Example: The Empirical Rule}

  What percentage of adult females have waist circumferences between
  32.5 inches and 39.5 inches?

  \vspace{4mm}

  Notice that $32.5 = 36 - 3.5 = \mu - \sigma$, and that $39.5 = 36 +
  3.5 = \mu + \sigma$. Therefore this is the interval $[\mu - \sigma,
    \mu + \sigma]$. Per the empirical rule, this interval contains
  68\% of adult females.
  
\end{frame}

\begin{frame}
  \frametitle{Example: The Empirical Rule}

  What percentage of adult females have waist circumferences between
  29 inches and 39.5 inches?

  \begin{center}
  \includegraphics[width=3.5in]{./figures/ch02/15_asymmetric.png}
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{Example: The Empirical Rule}

  What percentage of adult females have waist circumferences between
  29 inches and 39.5 inches?

  \vspace{4mm}

  Notice that $29 = 36 - 7 = \mu - 2\sigma$, and that $39.5 = 36 + 3.5
  = \mu + \sigma$. Therefore this is the interval $[\mu - 2\sigma, \mu
    + \sigma]$. However the empirical rule doesn't give us a statement
  for what this interval contains.
  
\end{frame}

\begin{frame}
  \frametitle{Example: The Empirical Rule}

  What percentage of adult females have waist circumferences between
  29 inches and 39.5 inches?

  \begin{center}
  \includegraphics[width=3.5in]{./figures/ch02/16_68.png}
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{Example: The Empirical Rule}

  What percentage of adult females have waist circumferences between
  29 inches and 39.5 inches?

  \begin{center}
  \includegraphics[width=3.5in]{./figures/ch02/17_two_pieces.png}
  \end{center}
  
\end{frame}


\begin{frame}
  \frametitle{Example: The Empirical Rule}

  What percentage of adult females have waist circumferences between
  29 inches and 39.5 inches?

  \begin{center}
  \includegraphics[width=3.5in]{./figures/ch02/18_two_pieces.png}
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{Example: The Empirical Rule}

  What percentage of adult females have waist circumferences between
  29 inches and 39.5 inches?

  \vspace{4mm}

  Therefore the content of the interval $[\mu - 2\sigma, \mu
    + \sigma]$ is $68\% + 13.5\% = 81.5\%$
  
\end{frame}

\begin{frame}
  \frametitle{The Relationship Between the Range and $\sigma$}

  By the empirical rule, 99.7\% of the population is within the band
  $[\mu - 3\sigma, \mu + 3\sigma]$. So on average, only 3 points out
  of 1000 will be outside this range.

  \vspace{4mm}

  For datasets smaller than about $n=1000$, we would expect the sample
  maximum to be near $\mu + 3\sigma$, and the sample min to be near
  $\mu - 3\sigma$. The space between them should thus be about
  $6\sigma$.

  \vspace{4mm}

  This leads to a rough estimate of the $\sigma$ using the range (max
  - min): $[\sigma \approx \frac{\mbox{Range}}{6}]$

  
\end{frame}

\begin{frame}
  \frametitle{Chebyshev's Rule}

  The empirical rule only works for normal distributions. Most
  populations don't have a normal distribution.

  \vspace{3mm}

  Chebyshev's rule is a counterpart to the empirical rule for
  non-normal distributions. However, it is a lower bound, whereas the
  empirical rule was an approximation.

  \vspace{3mm}

  Chebyshev's rule:

  \vspace{2mm}

  For \textbf{any} population or dataset, \underline{at least} $1 -
  \frac{1}{k^2}$ of the data lies in the interval $[\mu - k\sigma, \mu
    + k\sigma]$.
  
\end{frame}

\begin{frame}
  \frametitle{Example: Chebyshev's Rule}

  590 Survey respondents were asked this question:

  \vspace{1mm}

  \begin{center}
    ``How long do you expect to stay at your current job?''
  \end{center}

  \vspace{2mm}

  The mean of the responses was 10 years, and the standard deviation
  was 3 years. A histogram of the responses showed a pronounced right
  skew:

  \begin{center}
  \includegraphics[width=2in]{./figures/ch02/19_ln.png}
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{Example: Chebyshev's Rule}

  Estimate what proportion of respondents expect to stay in their
  current positions for between 4 and 16 years.

  \vspace{2mm}

  We need to find an interval in terms of $\mu$, $k$, and $\sigma$
  that spans $[4, 16]$. Then we can use $k$ to estimate the proportion
  with Chebyshev's rule.

  \vspace{2mm}

  $[4, 16] = [10 - 6, 10+6] = [10 - 2 \cdot 3, 10 + 2 \cdot 3] = [\mu -
    2\sigma, \mu + 2 \sigma]$

  \vspace{2mm}

  So $k=2$. Then by Chebyshev's rule, this interval contains at least
  $1 - \frac{1}{2^2} = 3/4$ of the survey respondents.
  
\end{frame}

\begin{frame}
  \frametitle{$z$ Scores}

  \begin{itemize}
  \item The $z$ score is a transformation that allows us to compare
    observations from different populations, in terms of magnitude.
  \item It's most appropriate when comparing observations from two
    different normal distributions.
  \item The $z$ score (or standardized score) for an observation X,
    coming from a distribution with mean $\mu$ and standard deviation
    $\sigma$ is:
  \end{itemize}

  $$z = \frac{X-\mu}{\sigma}$$  

\end{frame}


\begin{frame}
  \frametitle{$z$ Scores}

  \begin{itemize}
  \item If $X$ comes from a population with a normal distribution with
    mean $\mu$ and standard deviation $\sigma$, then the distribution
    of $z$ is normal with mean 0 and standard deviation 1.
  \item Thus, for normally distributed data, the $z$ score allows us
    to define another concrete definition of an outlier via the
    empirical rule: an outlier is any point with a $z$ score of less
    than -3, or more than +3.
  \end{itemize}

  $$z = \frac{X-\mu}{\sigma}$$  

\end{frame}


\begin{frame}
  \frametitle{Example: $z$ Scores}

  Consider two apartments: an apartment in Hartford with monthly rent
  of \$1436.55, and an apartment in NYC with monthly rent of
  \$2511.10. We can use a $z$ score to figure out which is the better
  deal, relative to its local market.

  \vspace{2mm}

  Suppose that monthly rent in Hartford is normally distributed, with
  $\mu = \$1042.50$ and $\sigma = \$152.25$. Suppose that monthly rent
  in NYC is normally distributed with $\mu = \$2016.50$ and $\sigma =
  \$294.60$.
  
\end{frame}

\begin{frame}
  \frametitle{Example: $z$ Scores}

  Then the $z$ score of the Hartford apartment is

  $$\frac{1436.55 - 1042.50}{152.25} = 2.59$$

  The $z$ score of the NYC apartment is

  $$\frac{2511.10 - 2016.50}{294.60} = 1.68$$

  \vspace{2mm}

  So even though the NYC apartment is more expensive in absolute
  terms, it's cheaper relative to its market. So the NYC apartment is
  the better deal.
  
\end{frame}



\end{document}

{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "d1c7ee8a-e33e-4c07-ba49-2692a504b182",
   "metadata": {},
   "source": [
    "# Probability"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "507aa9c1-4155-47c1-8b9c-9ab84f5d2c07",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import math\n",
    "from tqdm import tqdm\n",
    "from multiprocessing import Pool"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6ce9b68-4f2d-4d4d-a1e0-0b1e3259076d",
   "metadata": {},
   "source": [
    "## Simulating a \"Pick $k$ Numbers Out of $n$\"-Style Lottery"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba1bcdaa-accb-4fb2-9a3f-3c4dc1942bed",
   "metadata": {},
   "source": [
    "In the lecture, we saw how we can understand the probabilities of various outcomes of a \"pick 6 numbers out of 44\" lottery.\n",
    "\n",
    "Here we will simulate a similar lottery, i.e. a \"pick 4 numbers out of 20\" lottery. We need to examine this simpler situation due to computational constraints, but all of the mechanics are the same as the lottery we discussed in class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "b64dcc48-5560-4b94-aa73-bfb3825559e1",
   "metadata": {},
   "outputs": [],
   "source": [
    "total_numbers = 20\n",
    "n_you_pick = 4"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30800b72-e60f-4994-86b6-d025d8e8041f",
   "metadata": {},
   "source": [
    "This function makes tickets by drawing `n_you_pick` numbers from the first `total_numbers` integers, *without* replacement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "080b6120-0e07-4143-b04e-c8c65346250f",
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_ticket(max_number, n_numbers) -> set:\n",
    "    \"\"\"Randomly selects a set of `n_numbers` distinct integers between 1 and `max_number`.\"\"\"\n",
    "    \n",
    "    result = np.random.choice(range(1, max_number + 1), size=n_numbers, replace=False)\n",
    "    \n",
    "    return set(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9887f062-cdc5-4e8e-9361-10dadb1b1c11",
   "metadata": {},
   "source": [
    "Set the \"golden ticket\", i.e. the one that the lottery organizers generate and that players have to match in order to win."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "bd364f11-0d29-4430-870e-0aceda7d24a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "golden_ticket = make_ticket(total_numbers, n_you_pick)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a009d726-ba89-41f3-acda-796621811fb0",
   "metadata": {},
   "source": [
    "This function makes a new ticket, and counts how many numbers the ticket shares with the golden ticket."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "093fbd54-fe0c-4d8e-9130-2130908fe188",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_match_count(golden_ticket: set) -> int:\n",
    "    this_ticket = make_ticket(total_numbers,n_you_pick)\n",
    "    \n",
    "    return len(golden_ticket.intersection(this_ticket))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b8575749-a091-4a62-8228-077d22c83ac3",
   "metadata": {},
   "source": [
    "This loop constructs a large number of tickets and calculates how many numbers each one shares with the golden ticket."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "4b37d5c3-85e8-4945-9314-0122456f7e6b",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "100%|██████████| 10000000/10000000 [14:27<00:00, 11532.70it/s]\n"
     ]
    }
   ],
   "source": [
    "n = int(1e7)\n",
    "\n",
    "pool = Pool() \n",
    "match_counts = list(tqdm(pool.imap(get_match_count, [golden_ticket]*n), total=n))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec6c1365-0236-4234-9399-1e308a1ae553",
   "metadata": {},
   "source": [
    "The number of matches are tallied below. For simplicity we scale these tallies by the total number of possible tickets, so that we can directly compare the empirical frequencies with the number of possible ways that each match tally could occur."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "43845029-95ca-4f13-aa7a-ea52b389b610",
   "metadata": {},
   "outputs": [],
   "source": [
    "scaled_empirical_frequencies = pd.Series(match_counts).value_counts(normalize=True).sort_index() * math.comb(total_numbers, n_you_pick)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "c49d6eae-67c7-4a34-bfdf-80de2ac0787a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>scaled empirical frequency</th>\n",
       "      <th>theoretical frequency</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1815.886136</td>\n",
       "      <td>1820</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>2243.571728</td>\n",
       "      <td>2240</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>720.204405</td>\n",
       "      <td>720</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>64.512144</td>\n",
       "      <td>64</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>0.825588</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   scaled empirical frequency  theoretical frequency\n",
       "0                 1815.886136                   1820\n",
       "1                 2243.571728                   2240\n",
       "2                  720.204405                    720\n",
       "3                   64.512144                     64\n",
       "4                    0.825588                      1"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "pd.DataFrame({'scaled empirical frequency' : scaled_empirical_frequencies, \n",
    "              'theoretical frequency': [math.comb(n_you_pick, i) * math.comb(total_numbers - n_you_pick, n_you_pick-i) for i in range(0, n_you_pick+1)]})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f911ec12-fff2-4384-945e-323a3c887b9f",
   "metadata": {},
   "source": [
    "## The Rare Disease Detection Problem"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0da180df-b5a5-40a8-9168-f5a351494d12",
   "metadata": {},
   "source": [
    "Another example we explored in the lecture was the rare disease detection problem. \n",
    "\n",
    "Even a very accurate diagnostic test will struggle to yield a high probability of the patient having the disease given a positive test result, if the disease is rare. We saw that the root cause of this problem was that the rarer the disease, the higher the number of false positives, which may even exceed the true positives.\n",
    "\n",
    "In this section we will demonstrate this problem and explore its sensitivity to the rareness of the disease and the acuracy of the test."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc2ff89d-5f6b-4831-b77d-350c2adbc149",
   "metadata": {},
   "source": [
    "### Proof of Concept"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b26ef65-b040-44e4-829d-4e8eab5a28ae",
   "metadata": {},
   "source": [
    "First let's set some parameters for the PoC."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "id": "963de8d8-44f1-4331-a3bc-c8bdb159a385",
   "metadata": {},
   "outputs": [],
   "source": [
    "disease_incidence = 1/10\n",
    "test_sensitivity = 0.975 # probability that disease will be detected in a sick person\n",
    "test_specificity = 0.95 # probability that a healthy person will receive a negative result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "id": "21931489-249a-4568-ba0f-4ac388200970",
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_patient_with_test_result(disease_incidence, test_sensitivity, test_specificity) -> dict:\n",
    "    status = np.random.choice([\"Sick\", \"Healthy\"], p = [disease_incidence, 1 - disease_incidence])\n",
    "    \n",
    "    if status == 'Sick':\n",
    "        test_result = np.random.choice([\"Positive\", \"Negative\"], p = [test_sensitivity, 1 - test_sensitivity])\n",
    "        \n",
    "    if status == 'Healthy':\n",
    "        test_result = np.random.choice([\"Positive\", \"Negative\"], p = [1 - test_specificity, test_specificity])\n",
    "        \n",
    "    return {'actual health status': status, 'test result': test_result}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "id": "c829b511-ecd7-4e00-b9e1-6fa4c8f6a002",
   "metadata": {},
   "outputs": [],
   "source": [
    "patient_df = pd.DataFrame.from_dict([make_patient_with_test_result(disease_incidence, test_sensitivity, test_specificity) for i in range(int(1e6))])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c1c3ded3-044c-4cb9-bb0a-dca801e5b9a6",
   "metadata": {},
   "source": [
    "A patient who receives a positive test result will be interested in the probability that they actually have the disease. Since false positives are possible, this probability is less than 1. We can directly calculate it using the formula from the lecture:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "id": "671d0c05-d6c7-411b-9a4b-8191bbc09900",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6842105263157893"
      ]
     },
     "execution_count": 36,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "((disease_incidence) * (test_sensitivity)) / ((disease_incidence) * (test_sensitivity) + (1 - disease_incidence) * (1 - test_specificity))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8ae29b2-e188-4181-8460-6ad7c4410c91",
   "metadata": {},
   "source": [
    "Let's check that this is borne out by the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "id": "8274eef8-14b4-48a3-8d35-ebd87d69d5f2",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Sick       0.684495\n",
       "Healthy    0.315505\n",
       "Name: actual health status, dtype: float64"
      ]
     },
     "execution_count": 42,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "patient_df[patient_df['test result'] == 'Positive'][\"actual health status\"].value_counts(normalize=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a84bfd8c-bd24-495f-935c-bce0f3665f13",
   "metadata": {},
   "source": [
    "### A Truly Rare Disease"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26ff92e8-0c8e-4bf3-8b13-960430e3bab9",
   "metadata": {},
   "source": [
    "To test the sensitivity of $P(D | +)$ to the disease incidence, let's consider a truly rare disease that only affect one out of every one thousand people. We'll keep the test the same for now."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "id": "01e366ef-94fd-4217-b5a1-247bb9ff8afe",
   "metadata": {},
   "outputs": [],
   "source": [
    "disease_incidence = 1/1000\n",
    "test_sensitivity = 0.975 # probability that disease will be detected in a sick person\n",
    "test_specificity = 0.95 # probability that a healthy person will receive a negative result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "id": "ff51bd0b-d099-4eab-9af5-c647396a278d",
   "metadata": {},
   "outputs": [],
   "source": [
    "patient_df = pd.DataFrame.from_dict([make_patient_with_test_result(disease_incidence, test_sensitivity, test_specificity) for i in range(int(1e6))])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "id": "7a4511d1-8170-4d01-8853-266785889cef",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.019145802650957274"
      ]
     },
     "execution_count": 45,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "((disease_incidence) * (test_sensitivity)) / ((disease_incidence) * (test_sensitivity) + (1 - disease_incidence) * (1 - test_specificity))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "id": "f0027740-b1e7-40f4-9ad5-271a464ce0ee",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Healthy    0.980274\n",
       "Sick       0.019726\n",
       "Name: actual health status, dtype: float64"
      ]
     },
     "execution_count": 46,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "patient_df[patient_df['test result'] == 'Positive'][\"actual health status\"].value_counts(normalize=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67df8354-1700-4b5b-83d8-6eb9838d8934",
   "metadata": {},
   "source": [
    "### Improving the Test"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2bbb94c-f043-4349-bab0-057abcb12ac1",
   "metadata": {},
   "source": [
    "Considering this rare disease even further, what happens if we improve the test?\n",
    "\n",
    "Note that in reality, this change would require considerable investment and R&D, whereas here we can just change a variable's value. For some diseases effecting a change in the sensitivity from 98% to 99% may cost several million dollars and years of lab work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "id": "ebdd33c1-2283-45d1-aa0b-4941e8b2cfd2",
   "metadata": {},
   "outputs": [],
   "source": [
    "disease_incidence = 1/1000\n",
    "test_sensitivity = 0.999 # this is a *really* good test\n",
    "test_specificity = 0.995 # it's highly specific, too"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "id": "f6ea1366-cd12-4cdb-9799-709d658823ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "patient_df = pd.DataFrame.from_dict([make_patient_with_test_result(disease_incidence, test_sensitivity, test_specificity) for i in range(int(1e6))])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "id": "e96b0b78-e750-4448-98c4-71ceeaa5ccff",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.16666666666666655"
      ]
     },
     "execution_count": 57,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "((disease_incidence) * (test_sensitivity)) / ((disease_incidence) * (test_sensitivity) + (1 - disease_incidence) * (1 - test_specificity))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "id": "39c1c11c-04c2-4f95-90ec-93c818d49218",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Healthy    0.835945\n",
       "Sick       0.164055\n",
       "Name: actual health status, dtype: float64"
      ]
     },
     "execution_count": 58,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "patient_df[patient_df['test result'] == 'Positive'][\"actual health status\"].value_counts(normalize=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "51d152e4-8e08-4161-99ed-0351df2d83d9",
   "metadata": {},
   "source": [
    "The situation has improved a bit, but the odds of having the disease given a positive result are still really low. This drives home the fact that the problem is ultimately due to the rareness of the disease and the resulting sizes of the true positive and false positive classes of patients."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7eb5cab9-8896-4d9c-8303-c3c435a62f8b",
   "metadata": {},
   "outputs": [],
   "source": [
    "s"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

\documentclass[12pt]{article}
\usepackage{fullpage,amsmath,multicol}

\begin{document}

\begin{center}
\textbf{Final Exam Function Sheet}
\end{center}

This document describes the main functions we've used in the inference
section of our course. Below is a table of the parameters we have
built confidence intervals and hypothesis tests for, and the functions
used to produce them.

\begin{center}
\begin{tabular}{|c|c|c|}\hline
Target parameter(s) & Confidence Interval & Hypothesis Test\\ \hline
$\mu$ ($\sigma$ known) & ZInterval & Z-Test\\ \hline
$\mu$ ($\sigma$ unknown) & TInterval & T-Test\\ \hline
$p$ & 1-PropZInterval & 1-PropZTest\\ \hline
$\mu_1-\mu_2$ ($\sigma_1$, $\sigma_2$ known) & 2-SampZInterval &
2-SampZTest \\ \hline 
$\mu_1-\mu_2$ ($\sigma_1$, $\sigma_2$ unknown, pooled) &
2-SampTInterval & 2-SampTTest\\ \hline 
$\mu_1-\mu_2$ ($\sigma_1$, $\sigma_2$ unknown, unpooled) &
2-SampTInterval & 2-SampTTest\\ \hline 
$p_1-p_2$ & 2-PropZInterval & 2-PropZTest\\ \hline
\end{tabular}
\end{center}

\begin{flushleft}
\underline{Normal distribution functions}
\end{flushleft}

\begin{itemize}

\item[-] \textbf{normalcdf}: calculates the probability of a
  $\mbox{N}(\mu,\sigma)$ variable being in some interval $[a,b]$.\\
  Argument structure: $\mbox{normalcdf}(a,b,\mu,\sigma)$. $a$ is the
  left side of the interval, and $b$ is the right side of the
  interval. $\mu$ is the population mean, and $\sigma$ is the
  population standard deviation.\\ Note: frequently we want to
  calculate the probability that $X$, the normal variable in question,
  is less than some number or greater than some number, rather than in
  some interval. Since the domain of a normal variable is
  $(-\infty,\infty)$, this is equivalent to calculating the
  probability of $X$ being in an interval where one side is either
  $-\infty$ or $\infty$. For example, if the distribution of $X$ is
  $\mbox{N}(10,5)$, then $P(X<8)$ is equivalent to
  $P(-\infty<X<8)$. The issue is that the TI-83/84 do not have a symol
  representing $\infty$. We can instead construct a number so large
  that it is effectively $\infty$. When outputting very large or very
  small numbers, the calculator uses a kind of scientific notation
  that looks like this: xEy, where x is a decimal number and y is an
  integer. This represents $x\times 10^{y}$. To the calculator, the
  number $10^{99}$ is effectively $\infty$, so we can represent
  $\infty$ as 1E99 and $-\infty$ as $-$1E99. You can enter 1E99 by
  typing 1, then pressing the 2nd button and the comma button to get
  the E (\emph{this is not the same as the letter E}), then typing
  99.\\ Example: Suppose the heights of people in a certain state, in
  inches, are distributed as $N(67,4)$. Find the probability that a
  random person from that state is less than 62 inches tall. We can do
  this by calculating $\mbox{normcdf}(-1\mbox{E}99,62,67,4)\approx
  0.1056498$.

\item[-] \textbf{invNorm}: calculates quantiles of the normal
  distribution by returning a number $Y$ so that $P(X<Y)=p$.\\
  Argument structure: $\mbox{invNorm}(p,\mu,\sigma)$. $p$ is a decimal
  representation of the required quantile. $\mu$ and $\sigma$ are the
  population mean and standard deviation.\\ Example: Earthquakes in
  California, measured on the Richter scale, have a magnitude
  distribution that is approximately $N(5,1.3)$. A structural engineer
  wants to design a building that can withstand all but the strongest
  1\% of earthquakes. What is the maximum Richter scale magnitude that
  she needs to design the building to withstand? If $X$ is a random
  variable representing a random earthquake's Richter scale magnitude,
  we need to find the $Y$ such that $P(X>Y)=0.01$. Since the quantile
  function counts from the left and not the right, it will be easier
  if we calculate the $Y$ such that $P(X<Y)=0.99$ (this is equivalent
  to the previous statement). The solution to this problem is to
  calculate $\mbox{invNorm}(.99, 5,1.3)\approx 8.02425$, so the
  engineer should design the building to withstand a magnitude 8.02425
  earthquake. 

\end{itemize}

\begin{flushleft}
\underline{Confidence interval functions}
\end{flushleft}

\begin{itemize}
\item[-] \textbf{ZInterval}: this function is used to construct a confidence interval for $\mu$ when the generating population is normal and $\sigma$ is known.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}\footnote{All input structures in this document will be given as though Stats were selected; instructions are similar if Data is selected.}
\end{center}

\begin{flushleft}
Inpt: Select Data if you are loading sample data into a list, and Stats if you are providing sample statistics\\
$\sigma$: the population standard deviation\\
$\bar{x}$: the sample mean\\
n: the sample size\\
C-Level: the confidence level
\end{flushleft}

\begin{center}
\underline{Output Structure}
\end{center}

(a, b): the two sides of the interval\\
$\bar{x}$: the sample mean\\
n: the sample size\\\\\\\\\

\end{multicols}

\item[-] \textbf{TInterval}: this function is used to construct a confidence interval for $\mu$ when the generating population is not normal, but $n\geq 30$, or when the generating population is normal but $\sigma$ is unknown.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

\begin{flushleft}
Inpt: Data or Stats \\
$\bar{x}$: the sample mean\\
Sx: the sample standard deviation\\
n: the sample size\\
C-Level: the confidence level
\end{flushleft}

\begin{center}
\underline{Output Structure}
\end{center}

(a, b): the two sides of the interval\\
$\bar{x}$: the sample mean\\
Sx: the sample standard deviation\\
n: the sample size\\\\\\\

\end{multicols}

\item[-] \textbf{1-PropZInterval}: this function is used to construct a confidence interval for $p$.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

x: the number of ``yes" responses\\
n: the sample size\\
C-Level: the confidence level

\begin{center}
\underline{Output Structure}
\end{center}

(a, b): the two sides of the interval\\
$\hat{p}$: the sample proportion $\left(\frac{x}{n}\right)$\\
n: the sample size

\end{multicols}

\item[-] \textbf{2-SampZInterval}: this function is used to construct a confidence interval for $\mu_1-\mu_2$ when both $\sigma_1$ and $\sigma_2$ are known.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

Inpt: Data or Stats \\
$\sigma1$: standard deviation of population 1\\
$\sigma2$: standard deviation of population 2\\
$\bar{x}1$: the mean of sample 1\\
n1: the size of sample 1\\
$\bar{x}2$: the mean of sample 2\\
n2: the size of sample 2\\
C-Level: the confidence level\\

\begin{center}
\underline{Output Structure}
\end{center}

(a, b): the two sides of the interval\\
$\bar{x}1$: the mean of sample 1\\
$\bar{x}2$: the mean of sample 2\\
n1: the size of sample 1\\
n1: the size of sample 1\\\\\\\\

\end{multicols}

\item[-] \textbf{2-SampTInterval}: this function is used to construct a confidence interval for $\mu_1-\mu_2$ when both $\sigma_1$ and $\sigma_2$ are unknown.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

Inpt: Data or Stats \\
$\bar{x}1$: the mean of sample 1\\
Sx1: the standard deviation of sample 1\\
n1: the size of sample 1\\
$\bar{x}2$: the mean of sample 2\\
Sx2: the standard deviation of sample 2\\
n2: the size of sample 2\\
Pooled: Yes or no\\
C-Level: the confidence level\\

\begin{center}
\underline{Output Structure}
\end{center}

(a, b): the two sides of the interval\\
df: a degrees of freedom approximation; out of the scope of this course\\
$\bar{x}1$: the mean of sample 1\\
$\bar{x}2$: the mean of sample 2\\
Sx1: the standard deviation of sample 1\\
Sx2: the standard deviation of sample 2\\
n1: the size of sample 1\\
n2: the size of sample 2\\

\end{multicols}

\newpage

\item[-] \textbf{2-PropZInterval}: this function is used to construct a confidence interval for $p_1-p_2$.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

x1: the number of ``yes" responses in sample 1\\
n1: the size of sample 1\\
x2: the number of ``yes" responses in sample 2\\
n2: the size of sample 2\\
C-Level: the confidence level

\begin{center}
\underline{Output Structure}
\end{center}

(a, b): the two sides of the interval\\
$\hat{p}_1$: the sample proportion for sample 1\\
$\hat{p}_2$: the sample proportion for sample 2\\
n1: the size of sample 1\\
n2: the size of sample 2\\\\\

\end{multicols}

\end{itemize}

\begin{flushleft}
\underline{Hypothesis test functions}
\end{flushleft}

\begin{itemize}
\item[-] \textbf{Z-Test}: this function is used to do a hypothesis test for $\mu$ when the generating population is normal and $\sigma$ is known.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

\begin{flushleft}
Inpt: Data or Stats\\
$\mu_0$: the value of $\mu$ specified in $H_0$\\
$\sigma$: the population standard deviation\\
$\bar{x}$: the sample mean\\
n: the sample size\\
$\mu$: select the structure of $H_A$\\
C-Level: the confidence level
\end{flushleft}

\begin{center}
\underline{Output Structure}
\end{center}

The specified form of $H_A$\\
$z$: the test statistic\\
$p$: the p-value\\
$\bar{x}$: the sample mean\\
n: the sample size\\\\\

\end{multicols}

\item[-] \textbf{T-Test}: this function is used to do a hypothesis test for $\mu$ when the generating population is not normal, but $n\geq 30$, or when the generating population is normal but $\sigma$ is unknown.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

\begin{flushleft}
Inpt: Data or Stats \\
$\mu_0$: the value of $\mu$ specified in $H_0$\\
$\bar{x}$: the sample mean\\
Sx: the sample standard deviation\\
n: the sample size\\
$\mu$: select the structure of $H_A$\\
\end{flushleft}

\begin{center}
\underline{Output Structure}
\end{center}

The specified form of $H_A$\\
$t$: the test statistic\\
$p$: the p-value\\
$\bar{x}$: the sample mean\\
Sx: the sample standard deviation\\
n: the sample size

\end{multicols}

\newpage

\item[-] \textbf{1-PropZTest}: this function is used to do a hypothesis test for $p$.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

$p_0$: the value of $p$ specified in $H_0$\\
x: the number of ``yes" responses\\
n: the sample size\\
prop:  select the structure of $H_A$\\

\begin{center}
\underline{Output Structure}
\end{center}

The specified form of $H_A$\\
$z$: the test statistic\\
$p$: the p-value\\
$\hat{p}$: the sample mean\\
n: the sample size

\end{multicols}

\item[-] \textbf{2-SampZTest}: this function is used to do a hypothesis test for $\mu_1-\mu_2$ when both $\sigma_1$ and $\sigma_2$ are known.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

Inpt: Data or Stats \\
$\sigma1$: standard deviation of population 1\\
$\sigma2$: standard deviation of population 2\\
$\bar{x}1$: the mean of sample 1\\
n1: the size of sample 1\\
$\bar{x}2$: the mean of sample 2\\
n2: the size of sample 2\\
$\mu_1$:  select the structure of $H_A$\\

\begin{center}
\underline{Output Structure}
\end{center}

The specified form of $H_A$\\
$z$: the test statistic\\
$p$: the p-value\\
$\bar{x}1$: the mean of sample 1\\
$\bar{x}2$: the mean of sample 2\\
n1: the size of sample 1\\
n2: the size of sample 2\\\\\

\end{multicols}

\item[-] \textbf{2-SampTTest}: this function is used to construct a confidence interval for $\mu_1-\mu_2$ when both $\sigma_1$ and $\sigma_2$ are unknown.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

Inpt: Data or Stats \\
$\bar{x}1$: the mean of sample 1\\
Sx1: the standard deviation of sample 1\\
n1: the size of sample 1\\
$\bar{x}2$: the mean of sample 2\\
Sx2: the standard deviation of sample 2\\
n2: the size of sample 2\\
$\mu_1$:  select the structure of $H_A$\\
Pooled: Yes or no\\\\

\begin{center}
\underline{Output Structure}
\end{center}

The specified form of $H_A$\\
$t$: the test statistic\\
$p$: the p-value\\
df: a degrees of freedom approximation; out of the scope of this course\\
$\bar{x}1$: the mean of sample 1\\
$\bar{x}2$: the mean of sample 2\\
Sx1: the standard deviation of sample 1\\
Sx2: the standard deviation of sample 2\\
n1: the size of sample 1\\
n2: the size of sample 2

\end{multicols}

\newpage

\item[-] \textbf{2-PropZTest}: this function is used to do a hypothesis test for $p_1-p_2$.

\begin{multicols}{2}

\begin{center}
\underline{Input Structure}
\end{center}

x1: the number of ``yes" responses in sample 1\\
n1: the size of sample 1\\
x2: the number of ``yes" responses in sample 2\\
n2: the size of sample 2\\
$p1$: select the structure of $H_A$

\begin{center}
\underline{Output Structure}
\end{center}

The specified form of $H_A$\\
$z$: the test statistic\\
$p$: the p-value\\
$\hat{p}_1$: the sample proportion for sample 1\\
$\hat{p}_2$: the sample proportion for sample 2\\
n1: the size of sample 1\\
n2: the size of sample 2

\end{multicols}

\end{itemize}

\end{document}